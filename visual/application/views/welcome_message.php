<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>SDB</title>

	<style type="text/css">

	::selection { background-color: #E13300; color: white; }
	::-moz-selection { background-color: #E13300; color: white; }

	body {
		background-color: #fff;
		margin: 40px;
		font: 13px/20px normal Helvetica, Arial, sans-serif;
		color: #4F5155;
	}

	a {
		color: #003399;
		background-color: transparent;
		font-weight: normal;
	}

	h1 {
		color: #444;
		background-color: transparent;
		border-bottom: 1px solid #D0D0D0;
		font-size: 19px;
		font-weight: normal;
		margin: 0 0 14px 0;
		padding: 14px 15px 10px 15px;
	}

	code {
		font-family: Consolas, Monaco, Courier New, Courier, monospace;
		font-size: 12px;
		background-color: #f9f9f9;
		border: 1px solid #D0D0D0;
		color: #002166;
		display: block;
		margin: 14px 0 14px 0;
		padding: 12px 10px 12px 10px;
	}

	#body {
		margin: 0 15px 0 15px;
	}

	p.footer {
		text-align: right;
		font-size: 11px;
		border-top: 1px solid #D0D0D0;
		line-height: 32px;
		padding: 0 10px 0 10px;
		margin: 20px 0 0 0;
	}

	#container {
		margin: 10px;
		border: 1px solid #D0D0D0;
		box-shadow: 0 0 8px #D0D0D0;
	}
	</style>
	    <style>
      body, html { margin:0; padding:0; }
      #heatmapContainerWrapper { width:1200px; height:455px; margin:auto; background:rgba(0,0,0,.1); }
      #heatmapContainer { width:100%; height:100%;background-image: url(<?=base_url();?>assets/img/floor2.png);}
    </style>
	<script src="<?=base_url();?>assets/js/heatmap.js"></script>
	<script src="<?=base_url();?>assets/js/jquery.js"></script>

</head>
<body>

<div id="container">
	<h1>SDB</h1>
	<div id="heatmapContainerWrapper">  
		  <div id="heatmapContainer">

		  </div>
		<form method='post' action='<?php echo base_url();?>index.php/Welcome/add'>
			<input type="hidden" name="timestamp" value="<?php echo $timestamp ?>"/>
		<button type="submit" id="button" >Click Me!</button>
		</form>
	</div>

</div>
<script>
      window.onload = function() {
        var heatmap = h337.create({
          container: document.getElementById('heatmapContainer')
        });
        window.h = heatmap;
		var points = [];
		var len = <?php echo count($hasil)?>;
		var i = 0;
		<?php for ($i=0;$i < count($hasil["location_x"]);$i++) {?>
		  var point = {
		  	x: <?php echo $hasil["location_x"][$i]; ?>,
			y: <?php echo $hasil["location_y"][$i]; ?>,
			value: <?php echo $hasil["count"][$i]; ?>
		  };
		  points.push(point);
		<?php }?>
		// heatmap data format
		var data = { 
		  max: 0,
		  data: points 
		};

		// if you have a set of datapoints always use setData instead of addData
		// for data initialization
		h.setData(data);
      };
</script>
</body>
</html>
