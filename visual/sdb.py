import pandas as pd
import os as os
import sys as sys
import json as json

timestamp = str(sys.argv[1])
# timestamp = str("18/12/20 06:23:36")
# print(timestamp)
total_user = len(os.listdir("D:/Allen/xampp/htdocs/sdb2/traj"))
ready_sent_php = []
# print(data)

for i in range(1, total_user):
    try:
        data = pd.read_csv('D:/Allen/xampp/htdocs/sdb2/traj/Dest_Traj_'+str(i)+'.csv')
        data_clean = data[data.timeStamp == timestamp]
        data_clean = data_clean[['location_x', 'location_y']]
        new = pd.DataFrame({'location_x': [int(data_clean['location_x'].mean())], 'location_y': [int(data_clean['location_y'].mean())]})
        ready_sent_php.append(new)
    except:
        continue
ready_sent_php = pd.concat(ready_sent_php)
ready_sent_php = ready_sent_php.dropna(how='all')
ready_sent_php = pd.DataFrame({'count' : ready_sent_php.groupby( [ "location_x", "location_y"] ).size()}).reset_index()
# print(json.dumps(ready_sent_php))
ready_sent_php = ready_sent_php.to_json()
print(ready_sent_php)
