package cn.edu.zju.db.datagen.algorithm

import org.deeplearning4j.nn.graph.ComputationGraph
import org.deeplearning4j.nn.transferlearning.TransferLearning
import org.deeplearning4j.nn.api.OptimizationAlgorithm
import org.deeplearning4j.nn.weights.WeightInit
import org.deeplearning4j.nn.conf._
import org.deeplearning4j.nn.conf.layers._
import org.deeplearning4j.nn.conf.graph.rnn._
import org.deeplearning4j.nn.conf.inputs.InputType
import org.deeplearning4j.nn.conf.WorkspaceMode
import org.deeplearning4j.optimize.listeners.ScoreIterationListener
import org.deeplearning4j.datasets.iterator.MultipleEpochsIterator
import org.deeplearning4j.datasets.datavec.RecordReaderMultiDataSetIterator
import org.deeplearning4j.util.ModelSerializer

import org.datavec.api.transform._
import org.datavec.api.transform.transform.time.StringToTimeTransform
import org.datavec.api.transform.sequence.comparator.NumericalColumnComparator
import org.datavec.api.transform.transform.string.ConcatenateStringColumns
import org.datavec.api.transform.transform.doubletransform.MinMaxNormalizer
import org.datavec.api.transform.schema.Schema
import org.datavec.api.transform.metadata.StringMetaData
import org.datavec.api.records.reader.impl.csv.CSVRecordReader
import org.datavec.api.split.FileSplit
import org.datavec.spark.storage.SparkStorageUtils
import org.datavec.spark.transform.misc.StringToWritablesFunction
import org.datavec.spark.transform.SparkTransformExecutor
import org.datavec.api.transform.condition._
import org.datavec.api.transform.condition.column._
import org.datavec.api.transform.sequence.window.ReduceSequenceByWindowTransform
import org.datavec.api.transform.reduce.Reducer
import org.datavec.api.transform.reduce.AggregableColumnReduction
import org.datavec.api.transform.sequence.window.TimeWindowFunction
import org.datavec.api.transform.ops.IAggregableReduceOp
import org.datavec.api.transform.metadata.ColumnMetaData
import org.datavec.api.writable._
import org.datavec.hadoop.records.reader.mapfile.MapFileSequenceRecordReader
import org.apache.commons.lang3.ArrayUtils
import org.apache.spark.sql.functions._

import org.nd4j.linalg.api.ndarray.INDArray
import org.nd4j.linalg.dataset.api.MultiDataSetPreProcessor
import org.nd4j.linalg.dataset.api.MultiDataSet
import org.nd4j.linalg.lossfunctions.LossFunctions
import org.nd4j.linalg.activations.Activation
import org.nd4j.linalg.learning.config._
import org.nd4j.linalg.factory.Nd4j
import org.nd4j.linalg.indexing.BooleanIndexing
import org.nd4j.linalg.indexing.INDArrayIndex
import org.nd4j.linalg.indexing.NDArrayIndex._
import org.nd4j.linalg.indexing.conditions.Conditions

import org.apache.spark.api.java.function.Function
import org.apache.commons.io.FileUtils
import org.joda.time.DateTimeZone
import org.joda.time.format.DateTimeFormat

import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import scala.io.Source
import java.util.Random
import java.util.concurrent.TimeUnit
import java.io._
import java.net.URL

class DeepLearning {
  
  val cache = new File(System.getProperty("user.home"), "/.deeplearning4j")

  val dataFile = new File(cache, "/aisdk_20171001.csv")

  if (!dataFile.exists()) {
    val remote = "http://blob.deeplearning4j.org/datasets/aisdk_20171001.csv.zip"
    val tmpZip = new File(cache, "aisdk_20171001.csv.zip")
    tmpZip.delete() // prevents errors
    println("Downloading file...")
    FileUtils.copyURLToFile(new URL(remote), tmpZip)
    println("Decompressing file...")
    ArchiveUtils.unzipFileTo(tmpZip.getAbsolutePath(), cache.getAbsolutePath())
    tmpZip.delete()
    println("Done.")
  } else {
    println("File already exists.")
  }

  val raw = sqlContext.read
    .format("com.databricks.spark.csv")
    .option("header", "true") // Use first line of all files as header
    .option("inferSchema", "true") // Automatically infer data types
    .load(dataFile.getAbsolutePath)

  val positions = raw
    .withColumn("Timestamp", unix_timestamp(raw("# Timestamp"), "dd/MM/YYYY HH:mm:ss"))
    .select("Timestamp", "MMSI", "Longitude", "Latitude")

  positions.printSchema
  positions.registerTempTable("positions")

  val sequences = positions
    .rdd
    .map(row => (row.getInt(1), (row.getLong(0), (row.getDouble(3), row.getDouble(2))))) // a tuple of ship ID and timed coordinates
    .groupBy(_._1)
    .map(group => (group._1, group._2.map(pos => pos._2).toSeq.sortBy(_._1)))

  case class Stats(numPositions: Int, minTime: Long, maxTime: Long, totalTime: Long)

  val stats = sequences
    .map { seq =>
      val timestamps = seq._2.map(_._1).toArray
      Stats(seq._2.size, timestamps.min, timestamps.max, (timestamps.max - timestamps.min))
    }
    .toDF()
  stats.registerTempTable("stats")

  // our reduction op class that we will need shortly
  // due to interpreter restrictions, we put this inside an object
  object Reductions extends Serializable {
    class GeoAveragingReduction(val columnOutputName: String = "AveragedLatLon", val delim: String = ",") extends AggregableColumnReduction {

      override def reduceOp(): IAggregableReduceOp[Writable, java.util.List[Writable]] = {
        new AverageCoordinateReduceOp(delim)
      }

      override def getColumnsOutputName(inputName: String): java.util.List[String] = List(columnOutputName)

      override def getColumnOutputMetaData(newColumnName: java.util.List[String], columnInputMeta: ColumnMetaData): java.util.List[ColumnMetaData] =
        List(new StringMetaData(columnOutputName))

      override def transform(inputSchema: Schema) = inputSchema

      override def outputColumnName: String = null

      override def outputColumnNames: Array[String] = new Array[String](0)

      override def columnNames: Array[String] = new Array[String](0)

      override def columnName: String = null

      def getInputSchema(): org.datavec.api.transform.schema.Schema = ???

      def setInputSchema(x$1: org.datavec.api.transform.schema.Schema): Unit = ???
    }

    class AverageCoordinateReduceOp(val delim: String) extends IAggregableReduceOp[Writable, java.util.List[Writable]] {
      final val PI_180 = Math.PI / 180.0

      var sumx = 0.0
      var sumy = 0.0
      var sumz = 0.0
      var count = 0

      override def combine[W <: IAggregableReduceOp[Writable, java.util.List[Writable]]](accu: W): Unit = {
        if (accu.isInstanceOf[AverageCoordinateReduceOp]) {
          val r: AverageCoordinateReduceOp =
            accu.asInstanceOf[AverageCoordinateReduceOp]
          sumx += r.sumx
          sumy += r.sumy
          sumz += r.sumz
          count += r.count
        } else {
          throw new IllegalStateException(
            "Cannot combine type of class: " + accu.getClass)
        }
      }

      override def accept(writable: Writable): Unit = {
        val str: String = writable.toString
        val split: Array[String] = str.split(delim)
        if (split.length != 2) {
          throw new IllegalStateException(
            "Could not parse lat/long string: \"" + str + "\"")
        }
        val latDeg: Double = java.lang.Double.parseDouble(split(0))
        val longDeg: Double = java.lang.Double.parseDouble(split(1))
        val lat: Double = latDeg * PI_180
        val lng: Double = longDeg * PI_180
        val x: Double = Math.cos(lat) * Math.cos(lng)
        val y: Double = Math.cos(lat) * Math.sin(lng)
        val z: Double = Math.sin(lat)
        sumx += x
        sumy += y
        sumz += z
        count += 1
      }

      override def get(): java.util.List[Writable] = {
        val x: Double = sumx / count
        val y: Double = sumy / count
        val z: Double = sumz / count
        val longRad: Double = Math.atan2(y, x)
        val hyp: Double = Math.sqrt(x * x + y * y)
        val latRad: Double = Math.atan2(z, hyp)
        val latDeg: Double = latRad / PI_180
        val longDeg: Double = longRad / PI_180
        val str: String = latDeg + delim + longDeg
        List(new Text(str))
      }
    }
  }

  val schema = new Schema.Builder()
    .addColumnsString("Timestamp")
    .addColumnCategorical("VesselType")
    .addColumnsString("MMSI")
    .addColumnsDouble("Lat", "Lon") // will convert to Double later
    .addColumnCategorical("Status")
    .addColumnsDouble("ROT", "SOG", "COG")
    .addColumnInteger("Heading")
    .addColumnsString("IMO", "Callsign", "Name")
    .addColumnCategorical("ShipType", "CargoType")
    .addColumnsInteger("Width", "Length")
    .addColumnCategorical("FixingDevice")
    .addColumnDouble("Draught")
    .addColumnsString("Destination", "ETA")
    .addColumnCategorical("SourceType")
    .addColumnsString("end")
    .build()

  val transform = new TransformProcess.Builder(schema)
    .removeAllColumnsExceptFor("Timestamp", "MMSI", "Lat", "Lon")
    .filter(BooleanCondition.OR(new DoubleColumnCondition("Lat", ConditionOp.GreaterThan, 90.0), new DoubleColumnCondition("Lat", ConditionOp.LessThan, -90.0))) // remove erroneous lat
    .filter(BooleanCondition.OR(new DoubleColumnCondition("Lon", ConditionOp.GreaterThan, 180.0), new DoubleColumnCondition("Lon", ConditionOp.LessThan, -180.0))) // remove erroneous lon
    .transform(new MinMaxNormalizer("Lat", -90.0, 90.0, 0.0, 1.0))
    .transform(new MinMaxNormalizer("Lon", -180.0, 180.0, 0.0, 1.0))
    .convertToString("Lat")
    .convertToString("Lon")
    .transform(new StringToTimeTransform("Timestamp", "dd/MM/YYYY HH:mm:ss", DateTimeZone.UTC))
    .transform(new ConcatenateStringColumns("LatLon", ",", List("Lat", "Lon")))
    .convertToSequence("MMSI", new NumericalColumnComparator("Timestamp", true))
    .transform(
      new ReduceSequenceByWindowTransform(
        new Reducer.Builder(ReduceOp.Count).keyColumns("MMSI")
          .countColumns("Timestamp")
          .customReduction("LatLon", new Reductions.GeoAveragingReduction("LatLon"))
          .takeFirstColumns("Timestamp")
          .build(),
        new TimeWindowFunction.Builder()
          .timeColumn("Timestamp")
          .windowSize(1L, TimeUnit.HOURS)
          .excludeEmptyWindows(true)
          .build()))
    .removeAllColumnsExceptFor("LatLon")
    .build

  // note we temporarily switch between java/scala APIs for convenience
  val rawData = sc
    .textFile(dataFile.getAbsolutePath)
    .filter(row => !row.startsWith("# Timestamp")) // filter out the header
    .toJavaRDD // datavec API uses Spark's Java API
    .map(new StringToWritablesFunction(new CSVRecordReader()))

  // once transform is applied, filter sequences we consider "too short"
  // decombine lat/lon then convert to arrays and split, then convert back to java APIs
  val records = SparkTransformExecutor
    .executeToSequence(rawData, transform)
    .rdd
    .filter(seq => seq.size() > 7)
    .map { row: java.util.List[java.util.List[Writable]] =>
      row.map { seq => seq.map(_.toString).map(_.split(",").toList.map(coord => new DoubleWritable(coord.toDouble).asInstanceOf[Writable])).flatten }
    }
    .map(_.toList.map(_.asJava).asJava)
    .toJavaRDD

  val split = records.randomSplit(Array[Double](0.8, 0.2))

  val trainSequences = split(0)
  val testSequences = split(1)

  // for purposes of this notebook, you only need to run this block once
  val trainFiles = new File(cache, "/ais_trajectories_train/")
  val testFiles = new File(cache, "/ais_trajectories_test/")

  // if you want to delete previously saved data
  // FileUtils.deleteDirectory(trainFiles)
  // FileUtils.deleteDirectory(testFiles)

  if (!trainFiles.exists()) SparkStorageUtils.saveMapFileSequences(trainFiles.getAbsolutePath(), trainSequences)
  if (!testFiles.exists()) SparkStorageUtils.saveMapFileSequences(testFiles.getAbsolutePath(), testSequences)

  // set up record readers that will read the features from disk
  val batchSize = 48

  // this preprocessor allows for insertion of GO/STOP tokens for the RNN
  object Preprocessor extends Serializable {
    class Seq2SeqAutoencoderPreProcessor extends MultiDataSetPreProcessor {

      override def preProcess(mds: MultiDataSet): Unit = {
        val input: INDArray = mds.getFeatures(0)
        val features: Array[INDArray] = Array.ofDim[INDArray](2)
        val labels: Array[INDArray] = Array.ofDim[INDArray](1)

        features(0) = input

        val mb: Int = input.size(0)
        val nClasses: Int = input.size(1)
        val origMaxTsLength: Int = input.size(2)
        val goStopTokenPos: Int = nClasses

        //1 new class, for GO/STOP. And one new time step for it also
        val newShape: Array[Int] = Array(mb, nClasses + 1, origMaxTsLength + 1)
        features(1) = Nd4j.create(newShape: _*)
        labels(0) = Nd4j.create(newShape: _*)
        //Create features. Append existing at time 1 to end. Put GO token at time 0
        features(1).put(Array[INDArrayIndex](all(), interval(0, input.size(1)), interval(1, newShape(2))), input)
        //Set GO token
        features(1).get(all(), point(goStopTokenPos), all()).assign(1)
        //Create labels. Append existing at time 0 to end-1. Put STOP token at last time step - **Accounting for variable length / masks**
        labels(0).put(Array[INDArrayIndex](all(), interval(0, input.size(1)), interval(0, newShape(2) - 1)), input)

        var lastTimeStepPos: Array[Int] = null

        if (mds.getFeaturesMaskArray(0) == null) { //No masks
          lastTimeStepPos = Array.ofDim[Int](input.size(0))
          for (i <- 0 until lastTimeStepPos.length) {
            lastTimeStepPos(i) = input.size(2) - 1
          }
        } else {
          val fm: INDArray = mds.getFeaturesMaskArray(0)
          val lastIdx: INDArray = BooleanIndexing.lastIndex(fm, Conditions.notEquals(0), 1)
          lastTimeStepPos = lastIdx.data().asInt()
        }
        for (i <- 0 until lastTimeStepPos.length) {
          labels(0).putScalar(i, goStopTokenPos, lastTimeStepPos(i), 1.0)
        }
        //In practice: Just need to append an extra 1 at the start (as all existing time series are now 1 step longer)
        var featureMasks: Array[INDArray] = null
        var labelsMasks: Array[INDArray] = null

        if (mds.getFeaturesMaskArray(0) != null) { //Masks are present - variable length
          featureMasks = Array.ofDim[INDArray](2)
          featureMasks(0) = mds.getFeaturesMaskArray(0)
          labelsMasks = Array.ofDim[INDArray](1)
          val newMask: INDArray = Nd4j.hstack(Nd4j.ones(mb, 1), mds.getFeaturesMaskArray(0))
          // println(mds.getFeaturesMaskArray(0).shape())
          // println(newMask.shape())
          featureMasks(1) = newMask
          labelsMasks(0) = newMask
        } else {
          //All same length
          featureMasks = null
          labelsMasks = null
        }
        //Same for labels
        mds.setFeatures(features)
        mds.setLabels(labels)
        mds.setFeaturesMaskArrays(featureMasks)
        mds.setLabelsMaskArray(labelsMasks)
      }

    }
  }

  // because this is an autoencoder, features = labels
  val trainRR = new MapFileSequenceRecordReader()
  trainRR.initialize(new FileSplit(trainFiles))
  val trainIter = new RecordReaderMultiDataSetIterator.Builder(batchSize)
    .addSequenceReader("records", trainRR)
    .addInput("records")
    .build()
  trainIter.setPreProcessor(new Preprocessor.Seq2SeqAutoencoderPreProcessor)

  val testRR = new MapFileSequenceRecordReader()
  testRR.initialize(new FileSplit(testFiles))
  val testIter = new RecordReaderMultiDataSetIterator.Builder(batchSize)
    .addSequenceReader("records", testRR)
    .addInput("records")
    .build()
  testIter.setPreProcessor(new Preprocessor.Seq2SeqAutoencoderPreProcessor)

  val conf = new NeuralNetConfiguration.Builder()
    .optimizationAlgo(OptimizationAlgorithm.STOCHASTIC_GRADIENT_DESCENT)
    .iterations(1)
    .seed(123)
    .regularization(true)
    .l2(0.001)
    .weightInit(WeightInit.XAVIER)
    .updater(new AdaDelta())
    .inferenceWorkspaceMode(WorkspaceMode.SINGLE)
    .trainingWorkspaceMode(WorkspaceMode.SINGLE)
    .graphBuilder()
    .addInputs("encoderInput", "decoderInput")
    .setInputTypes(InputType.recurrent(2), InputType.recurrent(3))
    .addLayer("encoder", new GravesLSTM.Builder().nOut(96).activation(Activation.TANH).build(), "encoderInput")
    .addLayer("encoder2", new GravesLSTM.Builder().nOut(48).activation(Activation.TANH).build(), "encoder")
    .addVertex("laststep", new LastTimeStepVertex("encoderInput"), "encoder2")
    .addVertex("dup", new DuplicateToTimeSeriesVertex("decoderInput"), "laststep")
    .addLayer("decoder", new GravesLSTM.Builder().nOut(48).activation(Activation.TANH).build(), "decoderInput", "dup")
    .addLayer("decoder2", new GravesLSTM.Builder().nOut(96).activation(Activation.TANH).build(), "decoder")
    .addLayer("output", new RnnOutputLayer.Builder().lossFunction(LossFunctions.LossFunction.MSE).activation(Activation.SIGMOID).nOut(3).build(), "decoder2")
    .setOutputs("output")
    .build()

  val net = new ComputationGraph(conf)
  net.setListeners(new ScoreIterationListener(1))

  // pass the training iterator to fit() to watch the network learn one epoch of training
  net.fit(train)

  // we will pass our training data to an iterator that can handle multiple epochs of training
  val numEpochs = 150

  (1 to numEpochs).foreach { i =>
    net.fit(trainIter)
    println(s"Finished epoch $i")
  }

  val modelFile = new File(cache, "/seq2seqautoencoder.zip")
  // write to disk
  ModelSerializer.writeModel(net, modelFile, false)
  // restore from disk
  val net = ModelSerializer.restoreComputationGraph(modelFile)

  def arr2Dub(arr: INDArray): Array[Double] = arr.dup().data().asDouble()
  val format = new java.text.DecimalFormat("#.##")

  testIter.reset()
  (0 to 10).foreach { i =>
    val mds = testIter.next(1)
    val reconstructionError = net.score(mds)
    val output = net.feedForward(mds.getFeatures(), false)
    val feat = arr2Dub(mds.getFeatures(0))
    val orig = feat.map(format.format(_)).mkString(",")
    val recon = arr2Dub(output.get("output")).map(format.format(_)).take(feat.size).mkString(",")

    println(s"Reconstruction error for example $i is $reconstructionError")
    println(s"Original array:        $orig")
    println(s"Reconstructed array:   $recon")
  }

  // use the GraphBuilder when your network is a ComputationGraph
  val encoder = new TransferLearning.GraphBuilder(net)
    .setFeatureExtractor("laststep")
    .removeVertexAndConnections("decoder-merge")
    .removeVertexAndConnections("decoder")
    .removeVertexAndConnections("decoder2")
    .removeVertexAndConnections("output")
    .removeVertexAndConnections("dup")
    .addLayer("output", new ActivationLayer.Builder().activation(Activation.IDENTITY).build(), "laststep")
    .setOutputs("output")
    .setInputs("encoderInput")
    .setInputTypes(InputType.recurrent(2))
    .build()

  // grab a single batch to test feed forward
  val ds = testIter.next(1)
  val embedding = encoder.feedForward(ds.getFeatures(0), false)
  val shape = embedding.get("output").shape().mkString(",")
  val dsFeat = arr2Dub(ds.getFeatures(0))
  val dsOrig = dsFeat.map(format.format(_)).mkString(",")
  val rep = arr2Dub(embedding.get("output")).map(format.format(_)).take(dsFeat.size).mkString(",")

  println(s"Compressed shape:       $shape")
  println(s"Original array:        $dsOrig")
  println(s"Compressed array:      $rep")
}